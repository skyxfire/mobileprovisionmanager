//
//  AppDelegate.m
//  MobileProvisionManager
//
//  Created by skyxfire on 10/27/16.
//  Copyright © 2016 skyxfire. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
