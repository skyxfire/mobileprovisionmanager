//
//  main.m
//  MobileProvisionManager
//
//  Created by skyxfire on 10/27/16.
//  Copyright © 2016 skyxfire. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
